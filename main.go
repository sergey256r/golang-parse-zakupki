package main

import (
	"gos_zakupki/database"
	"gos_zakupki/parser"
	"log"
	"strings"

	"github.com/ReflectiveRs/read-docx/docx"
)

func main() {
	log.Println("Старт загрузки закупок")

	err := database.CheckTableInBD() // Проверка наличие таблицы закупок в БД
	if err != nil {
		log.Println(err)
		return
	}

	zakupki, err := parser.GetZakupki() // Получить новые закупки
	if err != nil {
		log.Println(err)
		return
	}

	for _, zakupka := range zakupki {
		if database.CheckZakupkaInBD(&zakupka) {
			err := database.AddZakupka(&zakupka) // Добавляем новую закупку
			if err != nil {
				log.Println(err)
			} else {
				log.Printf("Добавлена новая закупка %s\n", zakupka.Number)
			}
		}

		if len(zakupka.Files) > 0 {
			database.AddZakupkaFiles(&zakupka) // Добавляем файлы к закупке
		}
	}

	// Получить файлы для загрузки
	filesZak, err := database.GetFilesForDown()
	if err != nil {
		log.Println(err)
		return
	}

	for _, file := range filesZak {
		if strings.HasSuffix(file.Name, ".docx") {
			document := docx.NewDocInUrl(file.Url)
			err := document.Read()
			if err != nil {
				log.Println(err)
			}
			database.AddZakupkaFileContent(file.Id, file.Url, document.GetContentText())
		} else {
			database.AddZakupkaFileContent(file.Id, file.Url, "none")
		}
	}

	log.Println("Загрузка закупок завершина")
}
