package database

import (
	"database/sql"
	"fmt"
	"gos_zakupki/zakupka"
	"log"

	_ "github.com/lib/pq"
)

var (
	db         *sql.DB
	connect_db string = "user=postgres password=qaz123 dbname=zakup sslmode=disable"
)

// Проверка наличие таблицы закупок в базе
func CheckTableInBD() error {
	var err error
	db, err = sql.Open("postgres", connect_db)
	if err != nil {
		return fmt.Errorf("нет подключения к БД: %s", err)
	}
	defer db.Close()

	_, err = db.Query("SELECT id FROM public.zakupki LIMIT 1")
	if err != nil {
		log.Println("Создание новой таблицы zakupki")
		err = createTable()
		if err != nil {
			return err
		}
		return nil
	}

	return nil
}

// Создание таблицы закупок в базе
func createTable() error {
	_, err := db.Exec("CREATE TABLE public.zakupki (id integer NOT NULL GENERATED ALWAYS AS IDENTITY(INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1), zakon text, number text, object text, customer text, customerlink text, price text, link text, date_place text, date_update text, data_end text, PRIMARY KEY(id) ); ALTER TABLE public.zakupki OWNER to postgres;")
	if err != nil {
		return fmt.Errorf("не получилось создать новую таблицу закупок: %s", err)
	}
	_, err = db.Exec("CREATE TABLE public.zakupki_files (id integer NOT NULL, name text COLLATE pg_catalog.\"default\",url text COLLATE pg_catalog.\"default\", content text COLLATE pg_catalog.\"default\") TABLESPACE pg_default; ALTER TABLE public.zakupki_files_old OWNER to postgres;")
	if err != nil {
		return fmt.Errorf("не получилось создать новую таблицу файлов-закупок: %s", err)
	}
	return nil
}

// Проверка существует ли закупка в базе
func CheckZakupkaInBD(z *zakupka.Zakupka) bool {
	var err error
	db, err = sql.Open("postgres", connect_db)
	if err != nil {
		log.Printf("CheckZakupkaInBD() нет подключения к БД: %s\n", err.Error())
		return true
	}
	defer db.Close()

	result, err := db.Query("SELECT COUNT(*) FROM public.zakupki WHERE number = $1", z.Number)
	if err != nil {
		log.Printf("CheckZakupkaInBD() закупка не существует в БД %s\n", err)
		return true
	}
	var count int
	result.Next()
	result.Scan(&count)

	return count == 0
}

// Добавить новую закупку
func AddZakupka(z *zakupka.Zakupka) error {
	var err error
	db, err = sql.Open("postgres", connect_db)
	if err != nil {
		return fmt.Errorf("нет подключения к БД: %s", err)
	}
	defer db.Close()

	_, err = db.Exec("INSERT INTO public.zakupki (zakon, number, object, customer, customerlink, price, link, date_place, date_update, data_end)"+
		" VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)", z.Zakon, z.Number, z.Object, z.Customer, z.CustomerLink, z.Price, z.Link, z.DatePlace, z.DateUpdate, z.DataEnd)
	if err != nil {
		return fmt.Errorf("не получилось добавить новую закупку %s Error: %s", z.Number, err)
	}

	return nil
}

// Добавляем файлы к закупке
func AddZakupkaFiles(z *zakupka.Zakupka) error {
	var err error
	db, err = sql.Open("postgres", connect_db)
	if err != nil {
		return fmt.Errorf("нет подключения к БД: %s", err)
	}
	defer db.Close()

	// Получить id закупки
	var id string
	res, err := db.Query("SELECT (id) FROM public.zakupki WHERE number = $1", z.Number)
	if err != nil {
		return fmt.Errorf("не найден id закупки %s Error: %s", z.Number, err)
	}
	res.Next()
	res.Scan(&id)

	for _, file := range z.Files {
		_, err = db.Exec("INSERT INTO public.zakupki_files (id, name, url) VALUES($1, $2, $3)", id, file.Name, file.Url)
		if err != nil {
			log.Printf("не могу добавить файл закупки %s\t id= %s\nError: %s\n", z.Number, id, err.Error())
		}
	}

	return nil
}

// Получить файлы для загрузки контента
func GetFilesForDown() ([]zakupka.File, error) {
	var err error
	var files []zakupka.File

	db, err = sql.Open("postgres", connect_db)
	if err != nil {
		return files, fmt.Errorf("нет подключения к БД: %s", err)
	}
	defer db.Close()

	result, err := db.Query("SELECT id, name, url FROM public.zakupki_files WHERE content is NULL")
	if err != nil {
		return files, fmt.Errorf("не могу получить список файлов закупок для загрузки: %s", err)
	}

	for result.Next() {
		var file zakupka.File
		err := result.Scan(&file.Id, &file.Name, &file.Url)
		if err != nil {
			log.Println(err)
			continue
		}
		files = append(files, file)
	}

	return files, nil
}

// Добавить контент файла к закупке
func AddZakupkaFileContent(id, url, content string) error {
	var err error
	db, err = sql.Open("postgres", connect_db)
	if err != nil {
		return fmt.Errorf("нет подключения к БД: %s", err)
	}
	defer db.Close()

	_, err = db.Exec("UPDATE public.zakupki_files SET content = $1 WHERE id = $2 AND url = $3", content, id, url)
	if err != nil {
		return fmt.Errorf("не могу добавить контент в закупку, %s", err)
	}

	return nil
}
