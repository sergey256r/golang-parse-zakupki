package parser

import (
	"testing"
)

func TestCorrectHref(t *testing.T) {
	urls := []string{"/epz/organization/view223/info.html?agencyId=1012",
		"/epz/organization/view223/info.html?agencyId=119929",
		"/epz/organization/view/info.html?organizationId=645700"}

	want := []string{"https://zakupki.gov.ru/epz/organization/view223/info.html?agencyId=1012",
		"https://zakupki.gov.ru/epz/organization/view223/info.html?agencyId=119929",
		"https://zakupki.gov.ru/epz/organization/view/info.html?organizationId=645700"}

	for i := 0; i < len(urls); i++ {
		if res := correctHref(urls[i]); res != want[i] {
			t.Errorf("TestHello() = %q,\n Want %q", res, want[i])
		}
	}
}
