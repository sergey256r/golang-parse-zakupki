package parser

import (
	"errors"
	"fmt"
	"gos_zakupki/zakupka"
	"log"
	"strconv"
	"strings"

	"github.com/anaskhan96/soup"
)

type parseOptions struct {
	pageNumbers int // Сколько страниц закупок парсить
	iteminPage  int // Сколько парсить закупок на одной странице (10, 20, 50, 100)
}

var ParseOptions = parseOptions{pageNumbers: 1, iteminPage: 10}

// Назначить сколько страниц закупок парсить
func (opt *parseOptions) SetPageNumbers(num int) error {
	if num > 20 {
		return errors.New("слишком большое число")
	}
	opt.pageNumbers = num
	return nil
}

// Сколько парсить закупок на одной странице (10, 20, 50, 100)
func (opt *parseOptions) SetIteminPage(num int) error {
	if num == 10 || num == 20 || num == 50 || num == 100 {
		opt.iteminPage = num
		return nil
	}
	return errors.New("парсить можно только 10, 20, 50, 100 закупок на странице")
}

func GetZakupki() ([]zakupka.Zakupka, error) {

	var zakupki []zakupka.Zakupka

	for pageNumber := 1; pageNumber <= ParseOptions.pageNumbers; pageNumber++ {

		resp, err := soup.Get("https://zakupki.gov.ru/epz/order/extendedsearch/results.html?morphology=on&search-filter=%D0%94%D0%B0%D1%82%D0%B5+%D1%80%D0%B0%D0%B7%D0%BC%D0%B5%D1%89%D0%B5%D0%BD%D0%B8%D1%8F&pageNumber=" + strconv.Itoa(pageNumber) + "&sortDirection=false&recordsPerPage=_" + strconv.Itoa(ParseOptions.iteminPage) + "&showLotsInfoHidden=false&sortBy=UPDATE_DATE&fz44=on&fz223=on&af=on&ca=on&pa=on&currencyIdGeneral=-1")
		if err != nil {
			return zakupki, err
		}
		doc := soup.HTMLParse(resp)

		// Закон, наименование
		var arrZakon []string
		fZakon := doc.FindAll("div", "class", "registry-entry__header-top__title")
		for _, zakon := range fZakon {
			// Чистим строку от пробелов
			str := strings.ReplaceAll(strings.ReplaceAll(strings.Trim(zakon.Text(), "\n "), " ", ""), "\n", " ")
			arrZakon = append(arrZakon, str)
		}

		// Номера закупок
		var arrNumber []string
		fNumber := doc.FindAll("div", "class", "registry-entry__header-mid__number")
		for _, number := range fNumber {
			arrNumber = append(arrNumber, strings.Trim(number.FullText(), "\n "))
		}

		// Обьект закупок
		var arrObject []string
		fObject := doc.FindAll("div", "class", "registry-entry__body-value")
		for _, object := range fObject {
			arrObject = append(arrObject, strings.Trim(object.Text(), "\n "))
		}

		// Заказчик и ссылка на заказчика
		var arrCustomer []string
		var arrCustomerLink []string
		fCustomer := doc.FindAll("div", "class", "registry-entry__body-href")
		for _, customer := range fCustomer {
			arrCustomer = append(arrCustomer, strings.Trim(customer.FullText(), "\n "))
			href := customer.Find("a").Attrs()["href"]
			arrCustomerLink = append(arrCustomerLink, correctHref(href))
		}

		// Цена
		var arrPrice []string
		fPrice := doc.FindAll("div", "class", "price-block__value")
		for _, price := range fPrice {
			// Чистим строку от пробелов
			str := strings.ReplaceAll(strings.ReplaceAll(strings.Trim(price.Text(), "\n "), " ", ""), "\n", " ")
			arrPrice = append(arrPrice, str)
		}

		// Ссылки на закупки
		var arrLink []string
		fLink := doc.FindAll("div", "class", "registry-entry__header-mid__number")
		for _, link := range fLink {
			href := link.Find("a").Attrs()["href"]
			arrLink = append(arrLink, correctHref(href))
		}

		// Даты
		var arrDatePlace []string  // Дата размещения
		var arrDateUpdate []string // Дата обновления
		var arrDataEnd []string    // Дата окончания

		fDate := doc.FindAll("div", "class", "data-block")
		for _, elem := range fDate {
			dates := elem.FindAll("div", "class", "data-block__value")
			arrDatePlace = append(arrDatePlace, dates[0].Text())
			arrDateUpdate = append(arrDateUpdate, dates[1].Text())
			arrDataEnd = append(arrDataEnd, dates[2].Text())

		}

		// Проверка информации
		if len(arrZakon) != ParseOptions.iteminPage && len(arrNumber) != ParseOptions.iteminPage && len(arrObject) != ParseOptions.iteminPage &&
			len(arrCustomer) != ParseOptions.iteminPage && len(arrCustomerLink) != ParseOptions.iteminPage && len(arrPrice) != ParseOptions.iteminPage &&
			len(arrLink) != ParseOptions.iteminPage && len(arrDatePlace) != ParseOptions.iteminPage {

			return zakupki, errors.New("количество полученных элементов со страницы разное")
		}

		// Заполняем информацией о закупках
		for k := 0; k < ParseOptions.iteminPage; k++ {
			// Получить файлы закупки
			zakupkaFiles, err := getZakupkaFiles(strings.Replace(arrLink[k], "common-info", "documents", 1), arrZakon[k])
			if err != nil {
				log.Println(err)
			}

			zakupki = append(zakupki, zakupka.Zakupka{arrZakon[k], arrNumber[k], arrObject[k], arrCustomer[k], arrCustomerLink[k], arrPrice[k],
				arrLink[k], arrDatePlace[k], arrDateUpdate[k], arrDataEnd[k], zakupkaFiles})
		}
	}

	return zakupki, nil
}

func correctHref(href string) string {
	if !strings.HasPrefix(href, "https") {
		href = "https://zakupki.gov.ru" + href
	}
	return href
}

func getZakupkaFiles(url, zakon string) ([]zakupka.File, error) {
	var files []zakupka.File

	resp, err := soup.Get(url)
	if err != nil {
		return files, fmt.Errorf("нет подключения для загрузки файлов, url: %s", err)
	}
	doc := soup.HTMLParse(resp)

	var elem []soup.Root

	if strings.Contains(zakon, "44-") {
		elem = doc.FindAll("span", "class", "section__value")
		for _, val := range elem {
			files = append(files, zakupka.File{"", val.Find("a").Attrs()["title"], val.Find("a").Attrs()["href"]})
		}

	} else if strings.Contains(zakon, "223-") {
		elem = doc.FindAll("a", "class", "epz_aware")
		for _, val := range elem {
			files = append(files, zakupka.File{"", strings.Trim(val.Text(), "\n "), correctHref(strings.Trim(val.Attrs()["href"], " "))})
		}

	} else {
		return files, fmt.Errorf("ссылки на файлы в закупке не найдены url: %s", url)
	}

	return files, nil
}
