package zakupka

type Zakupka struct {
	Zakon        string
	Number       string
	Object       string
	Customer     string
	CustomerLink string
	Price        string
	Link         string
	DatePlace    string
	DateUpdate   string
	DataEnd      string
	Files        []File
}

type File struct {
	Id   string
	Name string
	Url  string
}
