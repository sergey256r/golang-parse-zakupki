module gos_zakupki

go 1.17

require (
	github.com/ReflectiveRs/read-docx v0.0.0-20211205160756-0c3b584147b0
	github.com/anaskhan96/soup v1.2.4
	github.com/lib/pq v1.10.4
)

require (
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/text v0.3.0 // indirect
)
